﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ZXing;
using ZXing.QrCode;

public class QRCode 
{
    public static string DecodeQRCode(Texture2D texture)
    {
        var reader = new BarcodeReader();
        var pix = texture.GetPixels32();
        var result = reader.Decode(pix, texture.width, texture.height);
        return result.Text;
    }
    public static string DecodeQRCode(Color32[] pix, int width, int height)
    {
        var reader = new BarcodeReader();
        //var pix = texture.GetPixels32();
        var result = reader.Decode(pix, width, height);
        return result.Text;
    }
    

    public static Texture2D GenerateBarCode(string text) {
        var encoded = new Texture2D (256, 256);
        encoded.filterMode = FilterMode.Point;
        var color32 = EncodeBarCode(text, encoded.width, encoded.height);
        encoded.SetPixels32(color32);
        encoded.Apply();
        return encoded;
    }
    
    public static Texture2D GenerateQR(string text) {
        var encoded = new Texture2D (256, 256);
        var color32 = EncodeQR(text, encoded.width, encoded.height);
        encoded.SetPixels32(color32);
        encoded.Apply();
        return encoded;
    }
    private static Color32[] EncodeQR(string textForEncoding, 
        int width, int height) {
        var writer = new BarcodeWriter {
            Format = BarcodeFormat.QR_CODE,
            Options = new QrCodeEncodingOptions {
                Height = height,
                Width = width
            }
        };
        return writer.Write(textForEncoding);
    }
     private static Color32[] EncodeBarCode(string textForEncoding, 
        int width, int height) {
        var writer = new BarcodeWriter {
            Format = BarcodeFormat.CODE_128,
            Options = new QrCodeEncodingOptions {
                Height = height,
                Width = width
            }
        };
        return writer.Write(textForEncoding);
    }
    
    
}
