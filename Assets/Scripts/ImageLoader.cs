﻿using System.Collections;
using System.IO;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class ImageLoader : MonoBehaviour
{
//    private string defImage = "https://dapp.dblog.org/img/default.jpg";
    
//    [SerializeField] private GameObject LoaderPrefab;
//    [SerializeField] 
    private bool isLoading = false;
    private bool useCache = false;
    private bool preserveAspect = true;
    private GameObject loader;
    public Image image;
    public Action<int> onPartnerClick;
    public bool isPartner = false;
    public int partnerIndex = 0;
    public Text nameField; 
    // private VideoLoader Video;
    private bool isMuted;
    public Action OnLoaded;
//    public int hashedImages = 0;
    private static Dictionary<string, Sprite> hashTexture = new Dictionary<string, Sprite>();

    private string plPrefsPrefix = "image_";
    
    void Awake()
    {

        Init();
    }

    private bool isInited = false;

    private void OnEnable()
    {
        if (isLoading && curUrl != "")
        {
            LoadImage(curUrl);
        }
    }
    public void OnClick()
    {
        if (isPartner)
            onPartnerClick?.Invoke(partnerIndex);
        else
            OpenCode();
    }
    public void OpenCode()
    {
        string[] urlParts = curUrl.Split('/');
        string[] urlParts2 = urlParts[urlParts.Length - 1].Split('.');
        string code = urlParts2[0];
        print("OnClick code: " + code);
        DataHolder.Instance.GetModelConfig(code);
        HapticContoller.Haptic();
    }
    private void Init()
    {
        if(isInited) return;
        //===todo fix "?" problem on device
//        useCache = false;
        //=====

        // image = GetComponent<Image>();
        image.sprite = null;
        image.enabled = false;
        isInited = true;
    }

   
    private string curUrl = "";
//    private bool isVideo = false;
    
    // public void TryPlayVideo()
    // {
    //     if(Video != null)
    //         Video.PlayVedeo(curUrl, isMuted); 
    // }
    // public void TryStopVideo()
    // {
    //     if(Video != null)
    //         Video.Stop();
    //     if (loader != null)
    //         Destroy(loader.gameObject);


    // }
    public void LoadImage(string url, string pName = "", bool forceReload = false, bool isMuted = true)
    {
        if (pName == "") nameField.transform.parent.gameObject.SetActive(false);
        else
        {
            nameField.transform.parent.gameObject.SetActive(true);
            nameField.text = pName;
        }

        this.isMuted = isMuted;
        if (forceReload && hashTexture.ContainsKey(url))
        {
            hashTexture.Remove(url);
            curUrl = "";
            image.sprite = null;
        }
        isLoading = false;
        if (url == null) url = "";
        Init();
        image.enabled = false;
//        isVideo = false;
        
//         if (Video != null)
//             DestroyImmediate(Video.gameObject);

//         if (url.ToLower().Contains(".webm") || url.ToLower().Contains(".avi") || url.ToLower().Contains(".mp4") || url.ToLower().Contains(".mov"))
//         {
// //            isVideo = true;
//             GameObject VideoGO = Instantiate(
//                 isMuted ? UIController.instance.VideoLoaderPrefab : UIController.instance.VideoLoaderAudioPrefab,
//                 transform);

//             Video = VideoGO.GetComponent<VideoLoader>();
            
//             curUrl = url;
//             return;
//         }

        if (url == "")
        {
            // SetSprite(UIController.instance.DefImage);
            return;
        }

        if (image.sprite != null && curUrl == url)
        {
            image.enabled = true;
            return;
        }
        
        curUrl = url;
        image.sprite = null;
        
        if (hashTexture.ContainsKey(url))
        {
            SetSprite(hashTexture[url]);
            Debug.Log("[IMAGE_LOADER] use hashTexture");
            return;
        }
        else
        {
            
        }
        
        
        if(this.isActiveAndEnabled)
            StartCoroutine(DownloadImage(url));
        else
        {
            isLoading = true;
        }
    
    }

    void SetSprite(Sprite sprite)
    {
        image.enabled = true;
        image.sprite = sprite;
        image.preserveAspect = preserveAspect;
    }
//     IEnumerator LoadFromDisk(string url)
//     {
// //        if(loader == null)
// //            loader = Instantiate(UIController.instance.ImageLoaderPrefab, transform);

// //        yield return new WaitForSeconds(.3f);
//         // Start a download of the given URL
//         var www = new WWW(LoadFilePath(url));
//         yield return www;
// //        Destroy(loader);
//         Texture2D texture = www.texture;
       
//         www.LoadImageIntoTexture(texture);
        
//         Rect rec = new Rect(0, 0, texture.width, texture.height);
// //        image.color = new Color(1,1,1,0);
//         Sprite sprite =  Sprite.Create(texture, rec, new Vector2(0.5f,0.5f), 100);
// //        image.sprite = sprite;
// //        image.enabled = true;
//         if (!hashTexture.ContainsKey(url))
//         {
//             hashTexture.Add(url, sprite);
//         }
//         else
//         {
//             hashTexture[url] = sprite;
//         }

//         SetSprite(sprite);
// //        image.DOColor(Color.white, 1);

//     }
    
    IEnumerator DownloadImage(string url)
    {   
        // if(loader == null)
            // loader = Instantiate(UIController.instance.ImageLoaderPrefab, transform);
        isLoading = true;
        Debug.Log("GetTexture "+url);
        UnityWebRequest request = UnityWebRequestTexture.GetTexture(url);
        yield return request.SendWebRequest();
        isLoading = false;
        // Destroy(loader);
        
        if(request.isNetworkError || request.isHttpError) 
            Debug.Log(request.error);
        else
        {
            Texture2D texture = ((DownloadHandlerTexture) request.downloadHandler).texture;
            Rect rec = new Rect(0, 0, texture.width, texture.height);
//            image.enabled = true;
            SetSprite(Sprite.Create(texture,rec,new Vector2(0.5f,0.5f),100));
//            image.preserveAspect = preserveAspect;
            // image.DOColor(Color.white, 1);
            
            if (!hashTexture.ContainsKey(url))
            {
                hashTexture.Add(url, image.sprite);
            }
            else
            {
                hashTexture[url] = image.sprite;
            }
            
            
        }
        
        
    }

    
    // private void SaveTextureToFile(Texture2D texture, string  fileName)
    // {
    //     string filePath = SaveFilePath(fileName);
       
    //     Debug.Log("[IMAGE_LOADER] save image "+filePath );

    //     byte[] itemBGBytes = texture.EncodeToJPG();
    //     File.WriteAllBytes(filePath, itemBGBytes);
    // }
    // private string LoadFilePath(string urlFileName)
    // {
    //     string filePath = "";
    //     if (Application.platform == RuntimePlatform.IPhonePlayer)
    //     {
    //         filePath = Path.Combine("file://" + Application.streamingAssetsPath, Server.MD5Hash(urlFileName) + ".jpg");
    //     }
    //     else if (Application.platform == RuntimePlatform.Android)
    //     {
    //         filePath = Path.Combine("jar:file://" + Application.streamingAssetsPath + "!/assets/", Server.MD5Hash(urlFileName) + ".jpg");
    //     }
    //     else
    //     {
    //         filePath = Path.Combine("file://" + Application.streamingAssetsPath, Server.MD5Hash(urlFileName) + ".jpg");
    //     }

    //     return filePath ;
    // }
//     private string SaveFilePath(string urlFileName)
//     {
//         string filePath = "";
//         if (Application.platform == RuntimePlatform.IPhonePlayer)
//         {
//             filePath = Path.Combine(Application.streamingAssetsPath, Server.MD5Hash(urlFileName) + ".jpg");
//         }
// //        else if (Application.platform == RuntimePlatform.Android)
// //        {
// //            filePath = "jar:file://" + Application.streamingAssetsPath + "!/assets/";
// //        }
//         else
//         {
//             filePath = Path.Combine(Application.streamingAssetsPath, Server.MD5Hash(urlFileName) + ".jpg");
//         }

//         return filePath ;
//     }

    // public void OpenPopup()
    // {
    //     MediaPopup.Show(new MediaPopupData(new []{curUrl}, new []{""}));
    // }

    // public void Reset()
    // {
    //     image.sprite = null;
    //     curUrl = "";
    // }
}
