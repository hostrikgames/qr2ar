﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class BundleLoader : MonoBehaviour
{

    public static GameObject LoadedModel;

    public static bool IsLoaded = false;
    public Text loadingText;
    public Action OnLoaded;
    // public static BundleLoader FirstInstance;
    AssetBundle loadedBundle;
    //public Action OnBecameVisibleAction;
    //public Action OnBecameInVisibleAction;
    // public Transform InitConteiner;
    public Transform ObjConteiner;
    // public GameObject loadingCube;
    // public Mode DetectMode = Mode.Plane;
    public ReflectionProbe appReflProbe;
    public Light appLight;

    // public enum Mode{
    //     Plane = 1,
    //     Logo
    // }
    //private void OnBecameVisible()
    //{
    //    print("[BundleLoader instance] OnBecameVisible");
    //    OnBecameVisibleAction?.Invoke();
    //}
    //private void OnBecameInvisible()
    //{
    //    print("[BundleLoader instance] OnBecameInvisible");

    //    OnBecameInVisibleAction?.Invoke();
    //}
    private Animator anim;
    //public static string ProductId;
    private void Awake()
    {
       
        // print("[BundleLoader] Awake with Link " + DataHolder.Instance.GetBundleLink());

        // if (FirstInstance == null)
        // {
        // FirstInstance = this;
        string url = DataHolder.Instance == null  || DataHolder.Instance.GetBundleLink() == null ? null : DataHolder.Instance.GetBundleLink();

            LoadModel(url == null ? "https://locogames.fun/qr/osx/0001" : url);
            //LoadModel("https://locogames.fun/qr/AssetBundlesAndroid/3dobjects/chair");
            float intensity = 0;
            if(appLight != null && float.TryParse(DataHolder.Instance.AppLight, out intensity))
                appLight.intensity = intensity;
            if(appReflProbe != null && float.TryParse(DataHolder.Instance.AppReflaction , out intensity))
                appReflProbe.intensity = intensity;
        // }
        // else
        // {
        //     transform.localScale = Vector3.one;

        //     if (IsLoaded)
        //     {
        //         print("[BundleLoader] IsLoaded InstantiateModel");
        //         InstantiateModel();
        //     }
        //     else
        //     {
        //         print("[BundleLoader] Is not Loaded Waiting for OnLoaded");
        //         FirstInstance.OnLoaded = InstantiateModel;

        //     }
        // }
       

        
    }
    public void InitiateModel()
    {
        // loadingCube.SetActive(false);
        if(LoadedModel == null) {
            Debug.Log("InstantiateModel: LoadedModel is NUll");
            return;
        }

        Debug.Log("InstantiateModel : transform in " + ObjConteiner.name);
        GameObject loadedModel = Instantiate(LoadedModel, ObjConteiner);
        // loadedModel.transform.localPosition = DataHolder.ConvertStrToVec3(DetectMode == Mode.Logo ?
        //                                     DataHolder.Instance.ModelPositionLogo : DataHolder.Instance.ModelPositionPlane);

        // float scale = (DetectMode == Mode.Logo) ? float.Parse(DataHolder.Instance.ModelScaleLogo) : float.Parse(DataHolder.Instance.ModelScalePlane);
        // loadedModel.transform.localScale = Vector3.one * scale;
        if(DataHolder.Instance != null){

            var localPosition = DataHolder.ConvertStrToVec3(DataHolder.Instance.AppARType == DataHolder.ARType.logo ?
                                                DataHolder.Instance.ModelPositionLogo : DataHolder.Instance.ModelPositionPlane);
            print("localPosition" + localPosition);

            loadedModel.transform.localPosition = localPosition;
            var scale = float.Parse(DataHolder.Instance.AppARType == DataHolder.ARType.logo ?
                                                DataHolder.Instance.ModelScaleLogo : DataHolder.Instance.ModelScalePlane);
            print("Size & scale" +  DataHolder.Instance.ModelSize +" "+ scale); 
            loadedModel.transform.localScale =  DataHolder.ConvertStrToVec3(DataHolder.Instance.ModelSize) * scale;

            var localEulerAngles = DataHolder.ConvertStrToVec3(DataHolder.Instance.AppARType == DataHolder.ARType.logo ?
                                                DataHolder.Instance.ModelRotateLogo : DataHolder.Instance.ModelRotatePlane);
            print("localEulerAngles" + localEulerAngles);




            loadedModel.transform.localEulerAngles = localEulerAngles;

            string animations = DataHolder.Instance.GetAnimations;
            string[] animArray = animations.Split(',');

            if (animArray[0] != "")
            {
                AnimScript animScript = loadedModel.AddComponent<AnimScript>();
                animScript.Init(animArray);               
            }
            if (loadedModel.GetComponent<BoxCollider>() != null)
            {
                print("loadedModel.GetComponent<BoxCollider>" + loadedModel.GetComponent<BoxCollider>());
            }
            else {
                print("loadedModel.GetComponent<BoxCollider> == null" + loadedModel.GetComponent<BoxCollider>());
            }

        }

        // Light modelLight = loadedModel.GetComponentInChildren<Light>();
        // ReflectionProbe modelProbes = loadedModel.GetComponentInChildren<ReflectionProbe>();

        // if(modelLight != null)
        // {
        //     modelLight.intensity = DataHolder.Instance.ModelLight;
        // }
        // if(modelProbes != null)
        // {
        //     modelProbes.intensity = DataHolder.Instance.ModelReflaction;
        // }
    }
    
    public void LoadModel(string url)
    {
        print("[BundleLoader] StartCoroutine(InstantiateObject" + url);
        StartCoroutine(InstantiateObject(url));
    }
    Dictionary<string, string> cache = new Dictionary<string, string>();
    IEnumerator InstantiateObject(string url)
    {
        if (loadingText != null)
        {
            loadingText.gameObject.SetActive(true);
        }

        print("Load: " + url + "?r=" + UnityEngine.Random.Range(0, 10000));
        UnityWebRequest request = UnityWebRequestAssetBundle.GetAssetBundle(url, 0);

        StartCoroutine(Progress(request));

        yield return request.SendWebRequest();
        if (request.isNetworkError || request.isHttpError)
        {
            Debug.Log(request.error);
            if(loadingText != null) loadingText.text = "LOAD ERROR";
            
        }
        else
        {
            print("Loaded");
            if (loadingText != null)
            {
                loadingText.gameObject.SetActive(false);
            }
            AssetBundle bundle = DownloadHandlerAssetBundle.GetContent(request);

            print("AssetBundle null? " + (bundle == null));
            loadedBundle = bundle;
            LoadedModel = bundle.LoadAsset<GameObject>("model");
            // print("LoadedModel " + LoadedModel);
            IsLoaded = true;
            cache.Add(url, bundle.name);
            InitiateModel();
            OnLoaded?.Invoke();
        }
    }
    IEnumerator Progress(UnityWebRequest req)
    {
        //// Use isdone property of unitywebrequest no need to use additional variable
        while (!req.isDone)
        {
            if(loadingText != null) loadingText.text = "LOADING " + Mathf.RoundToInt(req.downloadProgress * 100) + "%";
            // Debug.Log("Downloading : " + req.downloadProgress * 100 + "%");
            yield return new WaitForSeconds(0.1f);
        }
        if(loadingText != null) loadingText.text = "LOADING 100%";
    }
    private void OnDestroy()
    {
        if (loadedBundle != null)
        {
            loadedBundle.Unload(true);
        }
    }
}
