﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseToTouches : MonoBehaviour {

	public float DeltaDistancePercentNoice = 10f;
	private Vector2 StartTouch1;
	private Vector2 StartTouch2;
	public Action OnTwoFingerDown;

	protected bool IsMultiTouch()
	{
		return Input.touchCount > 1;
	}

	protected float ZoomingDistance
	{
		get
		{
			if (Input.GetKeyDown(KeyCode.A))
			{
				return .01f;
			}
			if (Input.GetKeyDown(KeyCode.S))
			{
				return -.01f;
			}
			if (Input.touchCount > 1 && Input.touches[0].phase == TouchPhase.Moved 
			                         && Input.touches[1].phase == TouchPhase.Moved)
			{
				
				Vector2 prevTouch_0 = Input.touches[0].position - Input.touches[0].deltaPosition;
				Vector2 prevTouch_1 = Input.touches[1].position - Input.touches[1].deltaPosition;
				float dist =  Vector2.Distance(Input.touches[0].position, Input.touches[1].position)
					- Vector2.Distance(prevTouch_0, prevTouch_1);
				float distPercent =  dist / (float)Screen.height;
				
				if ((distPercent * distPercent) > (0.2f / (float)Screen.height) * (0.2f / (float)Screen.height)
				    && distPercent * distPercent < DeltaDistancePercentNoice * DeltaDistancePercentNoice)
				{
					return distPercent;
				}	
			}

			return 0;
		}
	}
	private Vector2 PrevDeltaPos = new Vector2(0, 0);
	
	protected Vector2 Delta()
	{
		Vector2 delta = Vector2.zero;
		if (Input.touchCount > 1 && Input.touches[0].phase == TouchPhase.Moved 
		    && Input.touches[1].phase == TouchPhase.Moved)
		{
			delta = Input.touches[0].position - PrevDeltaPos;
			PrevDeltaPos = Input.touches[0].position;
		}
		return delta;
	}

	protected Vector2 DragDelta()
	{
		if (Input.GetKeyDown(KeyCode.LeftArrow))
		{
			return Vector2.left*100f;
		}
		if (Input.GetKeyDown(KeyCode.RightArrow))
		{
			return Vector2.right*100f;
		}
		if (Input.GetKeyDown(KeyCode.UpArrow))
		{
			return Vector2.up*100f;
		}
		if (Input.GetKeyDown(KeyCode.DownArrow))
		{
			return Vector2.down*100f;
		}
		
		
		if (Input.touchCount > 1 && (Input.touches[0].phase == TouchPhase.Began ||
		    Input.touches[1].phase == TouchPhase.Began))
		{
			StartTouch1 = Input.touches[0].position;
			StartTouch2 = Input.touches[1].position;
		}
		if (Input.touchCount > 1 && Input.touches[0].phase == TouchPhase.Moved &&
		    Input.touches[1].phase == TouchPhase.Moved)
		{

//			float distance = Vector2.Distance(Input.touches[1].position , Input.touches[0].position);
//			print(distance / Screen.height);
//			if (distance / Screen.height > .2f) return Vector2.zero;
			
			
			Vector2 startTwoFigerMiddlePoint = StartTouch1 + (StartTouch2 - StartTouch1);
			Vector2 curTwoFingerMiddlePoint = Input.touches[0].position + (Input.touches[1].position - Input.touches[0].position);
			
			Vector2 delta = new Vector2(curTwoFingerMiddlePoint.x - startTwoFigerMiddlePoint.x, curTwoFingerMiddlePoint.y - startTwoFigerMiddlePoint.y);

			StartTouch1 = Input.touches[0].position;
			StartTouch2 = Input.touches[1].position;
			return delta;
		}
		return Vector2.zero;
	}
	protected Vector2 GetMultiTouchDistance()
	{
		
		return Input.touches[0].position - Input.touches[1].position;
	}
	protected bool GetMouseButtonDown()
	{
		
		if (Input.touchCount == 0)
		{
			return Input.GetMouseButtonDown(0);
		}
		else
		{
			PrevDeltaPos = Input.touches[0].position;
			
			if (Input.touches.Length >= 1)
			{
				StartTouch1 = Input.touches[0].position;
			}
			if (Input.touches.Length >= 2)
			{
				StartTouch1 = Input.touches[1].position;
			}
			
			return  (Input.touches[Input.touchCount - 1].phase == TouchPhase.Began);
		}

	}
	protected bool GetMouseButtonUp()
	{
		if (Input.touchCount == 0)
		{
			return Input.GetMouseButtonUp(0);
		}
		else
		{
			PrevDeltaPos = Input.touches[0].position;
			return  (Input.touches[Input.touchCount - 1].phase == TouchPhase.Ended);
		}
	}

	protected Vector3 mousePosition
	{
		get { 
			Vector3 mousepos;
     		if (Input.touchCount == 0)
     		{
     			mousepos = Input.mousePosition;
     		}
     		else
     		{
     			mousepos = Input.touches[Input.touchCount - 1].position;
     		}
     		return mousepos;
		}
	}
}
