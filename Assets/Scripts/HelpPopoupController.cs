﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class HelpPopoupController : MonoBehaviour
{
    public GameObject enVer;
    public GameObject ruVer;

    public UnityEvent OnEn;
    public UnityEvent OnRu;
    public void Reset()
    {
        SceneManager.LoadScene(1);
    }
    // Start is called before the first frame update
    void Start()
    {
        SetLang(DataHolder.CurLang);
        // enVer.SetActive(DataHolder.CurLang == "en");
        // ruVer.SetActive(DataHolder.CurLang == "ru");
    }
    private void OnEnable()
    {
        Screen.orientation = ScreenOrientation.Portrait;
    }
    private void OnDisable()
    {
        Screen.orientation = ScreenOrientation.AutoRotation;
    }
    public void SetLang(string lang)
    {
        DataHolder.CurLang = lang;
        if(lang== "en"){
            OnEn.Invoke();
        }else{
           OnRu.Invoke();  
        }
        enVer.SetActive(DataHolder.CurLang == "en");
        ruVer.SetActive(DataHolder.CurLang == "ru");
    }
}
