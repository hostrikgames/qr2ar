﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainSceneController : MonoBehaviour
{
    void Update()
    {
         if(Input.GetKeyDown(KeyCode.Escape))
            Application.Quit();
    }
    public void OpenContacts()
    {
        Application.OpenURL(DataHolder.Instance.ContactUsUrl);
    }

}
