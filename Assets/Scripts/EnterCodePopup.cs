﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnterCodePopup : MonoBehaviour
{
    [SerializeField] public InputField inputField;

    public void EnterCode()
    {
        if(inputField.text != "")
            DataHolder.Instance.LoadProductByCode(inputField.text);
    }
}
