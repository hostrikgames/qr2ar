﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
#if PLATFORM_ANDROID
using UnityEngine.Android;
#elif PLATFORM_IOS
using UnityEngine.iOS;
#endif

public class QRReaderController : MonoBehaviour
{
    // public GameObject loaderUI;
    public Text devText;
    public Text qrText;
    public GameObject rawImageGO;
    public RawImage rawImage;
    public RectTransform rawImageRT;
    public AspectRatioFitter rawImageARF;
    private WebCamTexture wct;
    bool isScaning = false;
    bool camInited = false;
    bool permissionRequared = true;
    private void Start()
    {
        rawImageGO = rawImage.gameObject;
        Application.targetFrameRate = 60;
        Screen.sleepTimeout = 120;

#if PLATFORM_ANDROID
        if (!Permission.HasUserAuthorizedPermission(Permission.Camera))
        {
            Permission.RequestUserPermission(Permission.Camera);
        }
#elif PLATFORM_IOS
        if (!Application.HasUserAuthorization(UserAuthorization.WebCam))
        {
            Application.RequestUserAuthorization(UserAuthorization.WebCam);
        }
#endif
            print("[SCANER] Start");

        //
        print("[SCANER] rawImage" + rawImage);
        print("[SCANER] wct " + wct);

    }
    public void ReInit() {
        camActive = true;
        camInited = false;
    }

    public void InitScanner()
    {
        if(wct == null){
            wct = new WebCamTexture();
            if(rawImage == null) rawImage = rawImageGO.AddComponent<RawImage>();
            rawImage.texture = wct;
            wct.Play();
            InvokeRepeating("Scan", .5f, 0.5f);
        }
    }
    bool camActive = false;
    private void Update()
    {
       
        if(!camActive) return;
            
        if (permissionRequared)
        {
#if PLATFORM_ANDROID
            if (Permission.HasUserAuthorizedPermission(Permission.Camera))
#elif PLATFORM_IOS
            if (Application.HasUserAuthorization(UserAuthorization.WebCam))
#endif
            {
                permissionRequared = false;
            }
        }
        else if(!camInited)
        {
            InitScanner();
            camInited = true;
            isScaning = true;
        }
        else if(camInited && isScaning)
        {
            if (wct.width < 100)
            {
                //Debug.Log("Still waiting another frame for correct info...");
                return;
            }

            // change as user rotates iPhone or Android:

            int cwNeeded = wct.videoRotationAngle;
            // Unity helpfully returns the _clockwise_ twist needed
            // guess nobody at Unity noticed their product works in counterclockwise:
            int ccwNeeded = -cwNeeded;

            // IF the image needs to be mirrored, it seems that it
            // ALSO needs to be spun. Strange: but true.
            if (wct.videoVerticallyMirrored) ccwNeeded += 180;

            // you'll be using a UI RawImage, so simply spin the RectTransform
            rawImageRT.localEulerAngles = new Vector3(0f, 0f, ccwNeeded);

            float videoRatio = (float)wct.width / (float)wct.height;

            // you'll be using an AspectRatioFitter on the Image, so simply set it
            rawImageARF.aspectRatio = videoRatio;

            // alert, the ONLY way to mirror a RAW image, is, the uvRect.
            // changing the scale is completely broken.
            if (wct.videoVerticallyMirrored)
                rawImage.uvRect = new Rect(1, 0, -1, 1);  // means flip on vertical axis
            else
                rawImage.uvRect = new Rect(0, 0, 1, 1);  // means no flip

            devText.text =
              ccwNeeded + "/" + videoRatio + "/" + wct.videoVerticallyMirrored;
        }

    }
    public void Scan()
    {
        qrText.text = "Scanning QR code...";
        var data = new Color32[wct.width * wct.height];
        wct.GetPixels32(data);
        
        try
        {
            var link = QRCode.DecodeQRCode(data, wct.width, wct.height);
            if(string.IsNullOrEmpty(link))
                Done(link);
        }
        catch (System.Exception ex)
        {

        }

    }
    public void StopVideo()
    {
        if(wct!=null){
            
            wct.Stop();
            rawImage.texture = null;
            DestroyObject(rawImage);
            DestroyObject(wct);
            wct = null;
            CancelInvoke("Scan");
            isScaning = false;
            camActive = false;
        }
    }
    private void Done(string link)
    {
        StopVideo();
        //qrText.text = "ANSWER: " + link;
        devText.text = "Done";
        DataHolder.Instance.GetModelConfig(link);
    }
}
