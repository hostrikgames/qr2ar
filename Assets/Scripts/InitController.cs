﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class InitController : MouseToTouches
{
    [SerializeField] ARSession m_Session;
    public  Transform sphereL;
    public  Transform sphereR;
    public  Transform sphereC;
    [SerializeField] ARCameraManager aRCameraManager;
    [SerializeField] Camera camera;
    [SerializeField] ARRaycastManager aRRaycastManager;
    [SerializeField] ARPlaneManager aRPlaneManager;
    //[SerializeField] ARReferencePointManager aRRefPointManager;
    [SerializeField] GameObject helpAnimation;
    // [SerializeField] GameObject prefabFirstInstance;
    [SerializeField] GameObject centerPlane;
    [SerializeField] Color centerPlaneGreen;
    [SerializeField] Text TapText;
    // [SerializeField] Text textObjActive;
    [SerializeField] Transform MoveHelpPoint;
    [SerializeField] GameObject HelpPopup;
    [SerializeField] RectTransform rotatedHalpHand;
    //[SerializeField] Color centerPlaneRed;

    [SerializeField] private UnityEvent OnSnapShotStart;
    [SerializeField] private UnityEvent OnSnapShotComplete;



    private float touchesSensivety = .3f;
    private float scaleSensivety = 1;
    private TrackableId curTrackableId;
    private bool isReady = false;
    private bool isPlaneDetected = false;
    private bool isObjVisible = true;
    private bool isFixed = true;

    private GameObject helpMovePointOnWall;

    private static bool IsResetedOnStart = false;
    string modelIsForWall = "";
    private void Awake()
    {
        
        //m_Session = FindObjectOfType<ARSession>();

        //if (m_Session != null)
        //{
        //    Destroy(m_Session.gameObject);
        //}
        //GameObject msGO = new GameObject("ARSession");
        //m_Session = msGO.AddComponent<ARSession>();
        //m_Session.enabled = false;

        Screen.orientation = ScreenOrientation.AutoRotation;

        camera.gameObject.SetActive(true);
        aRPlaneManager.planesChanged += planesChanged;
        aRPlaneManager.detectionMode = DataHolder.Instance.ModelIsForWall == "1" ? PlaneDetectionMode.Vertical : PlaneDetectionMode.Horizontal;
        
        Invoke("ShowHelpAnimation", 5);
        helpAnimation.SetActive(false);
        TapText.gameObject.SetActive(true);
        // TapText.text = "LOOK AROUND";
        UpdateText();

        if (PlayerPrefs.GetInt("help-popup-showed", 0) == 0)
        {
            HelpPopup.SetActive(true);
            PlayerPrefs.SetInt("help-popup-showed", 1);
        }
        if(DataHolder.Instance.ModelIsForWall == "1")
        {
            centerPlane.transform.eulerAngles = new Vector3(90, 0, 0);
            objContainer.gameObject.GetComponent<LookAtCamera>().enabled = false;
            
            helpMovePointOnWall = new GameObject("helppoint");
            helpMovePointOnWall.transform.SetParent(objContainer.transform);
            helpMovePointOnWall.transform.localPosition = Vector3.left;
        }
        if (DataHolder.Instance.ModelIsForWall == "1")
            rotatedHalpHand.localEulerAngles = Vector3.zero;
        // objContainer.gameObject.GetComponent<LookAtCamera>().enabled = DataHolder.Instance.ModelIsForWall == "0";
        modelIsForWall = DataHolder.Instance.ModelIsForWall;
    }

    public void UpdateText(){
        if(isPlaneDetected){
            if(DataHolder.CurLang == "en"){
                TapText.text = "TAP TO PUT OBJECT HERE";
            }else{
                TapText.text = "НАЖМИТЕ ЧТОБЫ ПОСТАВИТЬ 3Д-ОБЪКЕТ";
            }
        }else{
            if(DataHolder.CurLang == "en"){
                TapText.text = "LOOK AROUND TO DETECT PLACE";
            }else{
                TapText.text = "ОСМОТРИТЕСЬ ЧТОБЫ ОПРИДЕЛИТЬ МЕСТО";
            }
        }
    }
    private void planesChanged(ARPlanesChangedEventArgs obj)
    {
        print("[InitController] planesChanged added: " + obj.added.Count);
        if (obj.added.Count > 0)
        {
            UpdateText();
            centerPlane.transform.SetParent(aRPlaneManager.transform);
            centerPlane.transform.eulerAngles = DataHolder.Instance.ModelIsForWall == "1" ? new Vector3(90,0,0) : Vector3.zero;
            Renderer plane = centerPlane.GetComponentInChildren<Renderer>();
            plane.material = new Material(plane.material);
            plane.material.SetColor("_TintColor", centerPlaneGreen);
            isPlaneDetected = true;
            CancelInvoke("ShowHelpAnimation");
            helpAnimation.SetActive(false);
            aRPlaneManager.planesChanged -= planesChanged;

            Invoke("InitObject",.1f);
            
        }
    }
    void InitObject()
    {
        ShowInstance();
        RayCastUpdate2_obj();
        RayCastUpdate2();
    }

    IEnumerator Start()
    {
        if (ARSession.state == ARSessionState.None ||
            (ARSession.state == ARSessionState.CheckingAvailability))
        {
            yield return ARSession.CheckAvailability();
        }

        if (ARSession.state == ARSessionState.Unsupported)
        {
            // Start some fallback experience for unsupported devices
        }
        else
        {
            // Start the AR session

            m_Session.enabled = true;
            isReady = true;
            StartCoroutine(SlowUpdate());
        }
        // if(!IsResetedOnStart){
        //     IsResetedOnStart = true;

        //     SceneManager.LoadScene(1);
        // }else { 

        // }
        yield return new WaitForSeconds(1);
        m_Session.Reset();
    }

    private void ShowInstance()
    {
        objContainer.transform.localScale = Vector3.one;
        objContainer.gameObject.SetActive(true);
    }

    public void Reset()
    {
        m_Session.Reset();
    }

    bool isObjShowed = false;
    public GameObject objContainer;
    List<ARRaycastHit> raycastResult = new List<ARRaycastHit>();
    List<ARRaycastHit> raycastResult2 = new List<ARRaycastHit>();
    Vector3 targetDir;
    Vector3 mouseDownPos;
    bool isMouseDragged;
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            IsResetedOnStart = false;
            SceneManager.LoadScene(0);
        }
        //if (Input.GetMouseButtonUp(0))
        //{
        //    SnapButtonZone(Input.mousePosition);
        //}
            // textObjActive.text = "obj active: " + objContainer.activeSelf;
            if (!isReady || !isPlaneDetected) return;

        // RayCastUpdate2();
        if (Input.GetMouseButtonDown(0))
        {
            mouseDownPos = Input.mousePosition;
        }
        if (objContainer != null )
        {
            if (isObjVisible)
            {
                TapText.gameObject.SetActive(false);
                centerPlane.SetActive(false);
            }
            UpdateTouches();
        }
        if (Input.GetMouseButtonUp(0)) {
            //SnapButtonZone(Input.mousePosition);
            float dist = Vector3.Distance(Input.mousePosition, mouseDownPos);
            float Screenheight = (float)Screen.height;
            isMouseDragged = Vector3.Distance(Input.mousePosition, mouseDownPos)/ (float)Screen.height > .1f;
            print($"[POS] isMouseDragged {isMouseDragged} dist {dist} Screenheight {Screenheight}");
        }

        
        //set obj to center
        if (!isObjVisible && Input.GetMouseButtonUp(0) && !isMouseDragged && !SnapButtonZone(Input.mousePosition))
        {
            RayCastUpdate2_obj();
        }
        // if (Input.touches[0].phase == TouchPhase.Began) {
        //         isWasDraged = false;
        // }
        // if (Input.touches[0].phase == TouchPhase.Moved) { 
        //         isWasDraged = true;
        // }
        
        // if (Input.touchCount == 1 && Input.GetMouseButtonUp(0) )
        // {
        //     // if(!isWasDraged)
                
        //     // isFixed = !isFixed;
        // }
        RayCastUpdate2();
        // if (isReady && !isFixed) { 
        //     objContainer.transform.position = Vector3.Lerp(objContainer.transform.position ,objContainerPosTarget, Time.deltaTime);
        // }
    }
    private bool SnapButtonZone(Vector3 pos)
    {
        bool ver = pos.y < Screen.height / 5f;
        bool hor = pos.x > Screen.width /3 && pos.x < Screen.width - Screen.width / 3;
        print(ver && hor);
        return ver && hor;
    }
    public void SnapShot()
    {
        StartCoroutine("SnapShotFlow");
    }
    IEnumerator SnapShotFlow()
    {
        bool isPlaneWisible = centerPlane.gameObject.activeSelf;
        if (isPlaneWisible)
        {
            centerPlane.gameObject.SetActive(false);
        }
        OnSnapShotStart.Invoke();
        NativeShare share = new NativeShare();
        ScreenshotHelper.iCaptureScreen((texture2D) =>
        {
            share.SetSubject("https://ar-go.app.link?" + DataHolder.ProductId);
            //share.SetText("https://ar-go.app.link?" + DataHolder.ProductId);
            share.AddFile(texture2D);
            OnSnapShotComplete.Invoke();
        });
        
        yield return new WaitForSeconds(1);
        share.Share();
        if (isPlaneWisible)
        {
            centerPlane.gameObject.SetActive(true);
        }
        yield break;
    }
    Vector3 objContainerPosTarget;
    List<ARRaycastHit> raycastResultPlane = new List<ARRaycastHit>();
    void RayCastUpdate0() 
    {
        if (!isFixed) {
            if (aRRaycastManager.Raycast(new Vector3(Screen.width / 2f, Screen.height / 2, 0), raycastResultPlane)) 
            {
                objContainerPosTarget = raycastResultPlane[0].pose.position;
                
            }
        }else if(!isObjVisible){
            if (aRRaycastManager.Raycast(new Vector3(Screen.width / 2f, Screen.height / 2, 0), raycastResultPlane)) 
            {
                 centerPlane.transform.position = raycastResultPlane[0].pose.position;
            }
         }
    }
    List<GameObject> dots =new List<GameObject>();
    void RayCastUpdate2()
    {
        if (isObjVisible) return;
        //point L

        if (modelIsForWall == "1")
        {


            if (aRRaycastManager.Raycast(new Vector3(Screen.width / 1.3f, Screen.height / 2, 0), raycastResult))
            {
                sphereL.gameObject.SetActive(true);
                var item = raycastResult[0];
                sphereL.transform.position = item.pose.position;
            }


            //point R
            if (aRRaycastManager.Raycast(new Vector3(Screen.width / 3f, Screen.height / 2, 0), raycastResult))
            {
                sphereR.gameObject.SetActive(true);
                var item = raycastResult[0];
                sphereR.transform.position = item.pose.position;

                float angle = Mathf.Atan2(sphereR.position.z - sphereL.position.z, sphereR.position.x - sphereL.position.x) * 180 / Mathf.PI;

                //if(!isObjVisible)
                //{
                //    objContainer.transform.eulerAngles = new Vector3(0, -angle, 0);
                //    objContainer.transform.position = objContainerPosTarget;
                //}
                //else
                //{


                centerPlane.transform.eulerAngles = new Vector3(90, -angle, 0);
            }
        }
        else
        {
            centerPlane.transform.eulerAngles = new Vector3(0, 0, 0);

        }
        //point C

        if (aRRaycastManager.Raycast(new Vector3(Screen.width / 2f, Screen.height / 2, 0), raycastResult))
        {
            sphereC.gameObject.SetActive(true);
            var item = raycastResult[0];
            sphereC.transform.position = item.pose.position;

            centerPlane.transform.position = raycastResult[0].pose.position;

        }

    }
    void RayCastUpdate2_obj()
    {
        objContainer.transform.position = centerPlane.transform.position;
        if (modelIsForWall == "1")
        {
            //point L
            if (aRRaycastManager.Raycast(new Vector3(Screen.width / 1.3f, Screen.height / 2, 0), raycastResult))
            {
                var item = raycastResult[0];
                sphereL.transform.position = item.pose.position;
            }

            //point R
            if (aRRaycastManager.Raycast(new Vector3(Screen.width / 3f, Screen.height / 2, 0), raycastResult))
            {
                var item = raycastResult[0];
                sphereR.transform.position = item.pose.position;

                float angle = Mathf.Atan2(sphereR.position.z - sphereL.position.z, sphereR.position.x - sphereL.position.x) * 180 / Mathf.PI;
                objContainer.transform.eulerAngles = new Vector3(0, -angle, 0);
            }
        }

        //point C
        // if (aRRaycastManager.Raycast(new Vector3(Screen.width / 2f, Screen.height / 2, 0), raycastResult))
        // {
        //     var item = raycastResult[0];
        //     sphereC.transform.position = item.pose.position;
        //     objContainer.transform.position = objContainerPosTarget;
        // }
    }
    void RayCastUpdate1()
    {
        if (aRRaycastManager.Raycast(new Vector3(Screen.width / 2, Screen.height / 2, 0), raycastResult))
        { 
            centerPlane.transform.localPosition = raycastResult[0].pose.position;

            if (aRRaycastManager.Raycast(new Vector3(Screen.width / 3f, Screen.height / 2, 0), raycastResult2))
            {

                targetDir = (raycastResult[0].pose.position - raycastResult2[0].pose.position);
                // Vector3 targetDir = other.transform.position - gameObject.transform.position;
                float angle = Vector3.Angle(targetDir, transform.up)+90;
                centerPlane.transform.eulerAngles = new Vector3(centerPlane.transform.eulerAngles.x, angle, centerPlane.transform.eulerAngles.z);

                //if (curTrackableId == null || (curTrackableId != null  && curTrackableId != raycastResult[0].trackableId         ))
                if (!objContainer.activeSelf)
                {
                    ShowInstance();
                    objContainer.transform.position = raycastResult[0].pose.position;
                    objContainer.transform.eulerAngles = new Vector3(0, angle, 0);
                }
                else
                {
                    if (!isObjVisible)
                    {
                        centerPlane.SetActive(true);
                        TapText.gameObject.SetActive(true);

                        if (Input.touchCount > 0 && Input.touches[0].phase == TouchPhase.Ended){
                            objContainer.transform.position = raycastResult[0].pose.position;
                            
                            // targetDir = (raycastResult[0].pose.position - raycastResult2[0].pose.position);
                            // forward = objContainer.transform.up;
                            // angle = Vector3.Angle(targetDir, forward)+90;
                            objContainer.transform.eulerAngles = new Vector3(0, angle, 0);

                        }
                    }
                }
            }

        }
    }

    IEnumerator SlowUpdate() {
        while (true)
        {
            if (isReady || isPlaneDetected) 
            {
                RayCastUpdate0();
                CheckPositionUpdate();
                if (objContainer.activeSelf)
                {
                    if (!isObjVisible) {
                        centerPlane.SetActive(true);
                        TapText.gameObject.SetActive(true);
                    }
                    else
                    {
                        TapText.gameObject.SetActive(false);
                        centerPlane.SetActive(false);
                    }
                }
                yield return new WaitForSeconds(.1f);
            }
        }
    }
    bool isWasDraged = false;
    //float prevZoom = 0;
    private void UpdateTouches()
    {
        //move
        if(Input.touchCount == 1)
        {
            var dist = Vector3.Distance(camera.transform.position, objContainer.transform.position);

            var deltaUnits = modelIsForWall ==  "0" ? 
                new Vector3(Input.touches[0].deltaPosition.x / (float)Screen.width, 0, (Input.touches[0].deltaPosition.y / (float)Screen.height) * 5f) :
                new Vector3(Input.touches[0].deltaPosition.x / (float)Screen.width, (Input.touches[0].deltaPosition.y / (float)Screen.height) * 5f, 0) ;

            var prevHelpPos = MoveHelpPoint.position;
            MoveHelpPoint.localPosition += deltaUnits * dist * touchesSensivety;// (touchesSensivety / ) * DataHolder.Instance.ObjSize;
            var helpDelta = MoveHelpPoint.position - prevHelpPos;

            objContainer.transform.position += modelIsForWall ==  "0" ? 
            new Vector3(helpDelta.x, 0, helpDelta.z) : 
            new Vector3(0, helpDelta.y, 0);

            if(modelIsForWall ==  "1"){

                objContainer.transform.position += (helpMovePointOnWall.transform.position - objContainer.transform.position).normalized * deltaUnits.x * dist * touchesSensivety;
            }
        }
        else if(IsMultiTouch())
        {
            /*scale* /
            objInstance.transform.localScale += new Vector3(1,1,1) * ZoomingDistance * scaleSensivety;
            /**/

            //rotate
            // if(DataHolder.Instance.ModelIsForWall ==  "0")
            {
                DetectTouchMovement.Calculate();
                if (Mathf.Abs(DetectTouchMovement.turnAngleDelta) > 0)
                {
                    Vector3 rotationDeg = Vector3.zero;
                    rotationDeg.y = -DetectTouchMovement.turnAngleDelta;
                    objContainer.transform.localEulerAngles += rotationDeg;
                }
            }
        }
    }
    private void CheckPositionUpdate()
    {
        //if (objContainer == null) return;
        Vector3 screenPoint = camera.WorldToViewportPoint(objContainer.transform.position);
        isObjVisible = screenPoint.z > 0 && screenPoint.x > 0 && screenPoint.x < 1 && screenPoint.y > 0 && screenPoint.y < 1;
    }


    public void OpenStartScene()
    {
        Screen.orientation = ScreenOrientation.Portrait;
        m_Session.Reset();
        // BundleLoader.FirstInstance = null;
        IsResetedOnStart = false;
        SceneManager.LoadScene(0);
    }

    private void ShowHelpAnimation()
    {
        helpAnimation.SetActive(true);
    }
    private void OnDestroy()
    {
        aRPlaneManager.planesChanged -= planesChanged;
    }
}
