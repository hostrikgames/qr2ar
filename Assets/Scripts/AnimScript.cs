﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimScript : MonoBehaviour
{
    // Start is called before the first frame update
    private string[] animArray;
    private int animIndex = 0;
    Vector3 startMousePos;

    public void Init(string[] animArray)
    {
        Debug.Log("animArray.lenght = " + animArray.Length);
        this.animArray = animArray;
    }

    void OnMouseDown()
    {
        startMousePos = Input.mousePosition;
    }

    private void OnMouseUp()
    {
        var dist = Vector3.Distance(startMousePos, Input.mousePosition);
        Debug.Log("OnMouseDown");
        if (dist / (float)Screen.height > .05f)
        {
            Debug.Log("Mouse was moved");
            return;
        }
        var animClip = animArray[animIndex];
        animIndex = (animIndex + 1) % animArray.Length;
        GetComponent<Animator>().Play(animClip);
    }

    




}
