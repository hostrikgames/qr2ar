﻿using System.Collections;
using System.Collections.Generic;
using TapticPlugin;
using UnityEngine;

public class HapticContoller : MonoBehaviour
{
    public void DoHaptic(){
        TapticManager.Impact(ImpactFeedback.Medium);
    }
    public static void Haptic(){
        TapticManager.Impact(ImpactFeedback.Medium);
    }
}
