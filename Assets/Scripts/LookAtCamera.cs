﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtCamera : MonoBehaviour
{
    [SerializeField] private bool updateEachFrame;
    private GameObject camera;
    
    // Start is called before the first frame update
    void Start()
    {
        camera = GameObject.Find("AR Camera");
        if(camera == null)
        {
            camera = FindObjectOfType<Camera>().gameObject;
        }
        Align();
    }
    void OnEnable()
    {
        Align();
    }
    private void Update()
    {
        if (updateEachFrame) Align();
    }
    public void Align()
    {
//        Vector3 targetPos = Camera.main.transform.position;
//        targetPos.y = 0;
        if(camera != null)
        {

            transform.LookAt(camera.transform.position);
            transform.localEulerAngles = new Vector3(0,transform.localEulerAngles.y,0);
        
        }else
        {
            Debug.LogAssertion("AR Camera found");
        }
    }
}
