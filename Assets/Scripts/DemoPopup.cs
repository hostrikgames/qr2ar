﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DemoPopup : MonoBehaviour
{
    public static Action<List<object>> onInit;
    [SerializeField] private GameObject prefab;
    [SerializeField] private GameObject backBtnPrefab;
    [SerializeField] private Transform conteiner;
    [SerializeField] private List<PartnerProps> partnersList;
    public static List<object> demoList;
    private List<GameObject> adddedItems = new List<GameObject>();
    //private Dictionary<string, object> partners = new Dictionary<string, object>();
    void Awake()
    {
        if(demoList == null){
            onInit = Init;
        }else
        {
            Init(demoList);
        }
    }
    private void Init(List<object> list)
    {
        demoList = list;
        print(demoList.Count);
        foreach (var item in list){
            Dictionary<string, object> partnerData = item as Dictionary<string, object>;

            PartnerProps partner = new PartnerProps();
            partner.pName = partnerData["name"] as string;
            partner.logo = partnerData["logo"] as string;
            partner.products = partnerData["products"] as List<object>;

            partnersList.Add(partner);
            
        }
        InitMainList();
    }
    public void InitMainList()
    {
        CleanItems();
        for (int i = 0; i < partnersList.Count; i++)
        {
            var item = partnersList[i];
            GameObject image = Instantiate(prefab, conteiner);
            ImageLoader imageLoader = image.GetComponent<ImageLoader>();
            imageLoader.LoadImage(item.logo);
            imageLoader.partnerIndex = i;
            imageLoader.isPartner = true;
            imageLoader.onPartnerClick = InitPartner;
            adddedItems.Add(image);
        }
    }
    private void InitPartner(int index)
    {
        CleanItems();
        GameObject backBtn = Instantiate(backBtnPrefab, conteiner);
        backBtn.GetComponent<Button>().onClick.AddListener(InitMainList);

        adddedItems.Add(backBtn);
        //image.GetComponent<ImageLoader>().
        foreach (var item in partnersList[index].products)
        {
            GameObject image = Instantiate(prefab, conteiner);
            Dictionary<string, object> product = item as Dictionary<string, object>;
            image.GetComponent<ImageLoader>().LoadImage(product["p"] as string, product["n"] as string);
            adddedItems.Add(image);
        }
    }
    private void CleanItems()
    {
        foreach (var item in adddedItems)
        {
            Destroy(item);
        }
        adddedItems.Clear();
    }
}
[System.Serializable]
public struct PartnerProps
{
    public string pName;
    public string logo;
    public List<object> products;
}
