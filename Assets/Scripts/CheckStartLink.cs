﻿using System.Collections;
using System.Collections.Generic;
using BranchThirdParty_MiniJSON;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CheckStartLink : MonoBehaviour
{
    void Start()
    {
        Branch.initSession(CallbackWithBranchUniversalObject);
    }

    void CallbackWithBranchUniversalObject(BranchUniversalObject universalObject,
                                                BranchLinkProperties linkProps,
                                                string error)
    {
        if (error != null)
        {
            System.Console.WriteLine("Error : "
                                    + error);
        }
        else
        {
            if(universalObject != null && universalObject.metadata != null){

                print("universalObject: " +universalObject.ToJsonString());
                print("universalObject.metadata: " +universalObject.metadata.ToJsonString());
                Dictionary<string, object> meta = Json.Deserialize(universalObject.metadata.ToJsonString()) as Dictionary<string, object>;
                
                if (meta.ContainsKey("~referring_link"))
                {
                    var url = (string)meta["~referring_link"];
                    var code = url.Split('?')[1];
                    Debug.Log("url: " + url);
                    Debug.Log("CODE: " + code);
                    // Scene scene = SceneManager.GetActiveScene();
                    // if(scene.name != "Scanner")
                    //     SceneManager.LoadScene(0);
                    DataHolder.Instance.GetModelConfig(url);
                }else if (meta.ContainsKey("+referrer"))
                {
                    var url = (string)meta["+referrer"];
                    var code = url.Split('?')[1];
                    Debug.Log("url: " + url);
                    Debug.Log("CODE: " + code);
                    // Scene scene = SceneManager.GetActiveScene();
                    // if(scene.name != "Scanner")
                    //     SceneManager.LoadScene(0);
                    DataHolder.Instance.GetModelConfig(url);
                }
                else
                {
                    Debug.Log("URL: not found");
                }
            }
        }
    }

   

}
