﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor.Networking;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class DataHolder : MonoBehaviour
{
    public TextAsset configTest;
    public static DataHolder Instance;
    public static string CurLang{
        set => PlayerPrefs.SetString("lang", value);
        get => PlayerPrefs.GetString("lang", "en");

    } //"ru"
    public static string ProductId = "";

    public static string serverUrl = "https://ar2qr.com/";
    public static string appConfigFile = "app-config-v2.json";

    private Dictionary<string, object> products;
    private Dictionary<string, object> appConfig;
    private float objSize = 1;
    public ARType AppARType = ARType.plane;
    public ARType ProductARType = ARType.universal;
    public enum ARType{
        none,
        plane,
        logo,
        universal
    }
    private void Awake()
    {
        if(Instance == null)
        {

            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            DestroyImmediate(gameObject);
        }
        
    }
    void Start(){
        Application.targetFrameRate = 60;
        StartCoroutine(LoadingAppConfig());
    }

    public void GetModelConfig(string qrCodeLink)
    {
        string[] linkParts = qrCodeLink.Split('?');
        DataHolder.ProductId = linkParts[linkParts.Length - 1];

        if(DataHolder.ProductId.Contains("L"))
            ProductARType = ARType.logo;
        else if(DataHolder.ProductId.Contains("P"))
            ProductARType = ARType.plane;
        else 
            ProductARType = ARType.universal;
        
        if(DataHolder.ProductId.Contains("L") || DataHolder.ProductId.Contains("P") || DataHolder.ProductId.Contains("U"))
            DataHolder.ProductId = DataHolder.ProductId.Substring(1, DataHolder.ProductId.Length - 1);
        
        print("ProductId: " + DataHolder.ProductId);
        print("ProductARType: " + ProductARType.ToString());
        StartCoroutine(LoadingModelConfig());
    }

    /** /
    {"products":
	    {
		    "id":"0001",
		    "shop_link": "",
		    "source": "https://locogames.fun/qr/AssetBundlesAndroid/3dobjects/chair",
		    "pos":"ground"
	    }
    }
    /**/
    public void LoadTestProduct() {
        ProductId = "0010";
        ProductARType = ARType.plane;
        StartCoroutine(LoadingModelConfig());
    }
     public void LoadProductByCode(string code) {
        ProductId = code;
        ProductARType = ARType.plane;
        StartCoroutine(LoadingModelConfig());
    }
    IEnumerator LoadingModelConfig()
    {
        Scene scene = SceneManager.GetActiveScene();
        if(scene.name != "Scanner")
                SceneManager.LoadScene(0);
                    

        while (appConfig == null)
        {
            yield return new WaitForSeconds(.1f);
        }

        string url = (appConfig["models_configs"] as string) + 
                    ProductId+".json?r="+UnityEngine.Random.Range(0,10000);
        
        

        UnityWebRequest www = UnityWebRequest.Get(url);
        yield return www.SendWebRequest();
        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
            SceneManager.LoadScene(0);

        }
        else
        {
            // Show results as text
            Debug.Log(www.downloadHandler.text);
            var dict = Json.Deserialize(www.downloadHandler.text) as Dictionary<string, object>;
            products = dict["product"] as Dictionary<string, object>;
            //objSize = float.Parse(products["size"].ToString());
            if(ProductARType == ARType.universal || ProductARType == AppARType){
                //yield return new WaitForSeconds(10);
                SceneManager.LoadScene(1);
            }
            else
            {
                Debug.Log("Wrong app");
                Application.OpenURL("https://argo-lite.app.link?L" + ProductId);
                SceneManager.LoadScene(0);
                //if(appConfig != null){
                    
                    //if(Application.platform == RuntimePlatform.Android)
                    //    Application.OpenURL(ProductARType == ARType.logo ? (appConfig["android_logo_app"] as string) : (appConfig["android_plane_app"] as string));
                    //else{
                    //    Application.OpenURL(ProductARType == ARType.logo ? (appConfig["ios_logo_app"] as string) : (appConfig["ios_plane_app"] as string));
                    //}
                    
                //}
            }     
            
        }
        
    }
    IEnumerator LoadingAppConfig()
    {
        string url = serverUrl + appConfigFile + "?r="+UnityEngine.Random.Range(0,10000);
        print("url " + url);
        UnityWebRequest www = UnityWebRequest.Get(url);
        yield return www.SendWebRequest();
        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
            if (Application.isEditor)
            {
                ParseJson(configTest.text);
            }
        }
        else
        {
            string text = www.downloadHandler.text;
            ParseJson(text);
        }
    }
    private void ParseJson(string text)
    {
        Debug.Log(text);
        var dict = Json.Deserialize(text) as Dictionary<string, object>;
        appConfig = dict["config"] as Dictionary<string, object>;

        List<object> demo = appConfig["demo"] as List<object>;
        print("demo.Count" + demo.Count);
        DemoPopup.onInit?.Invoke(demo);
    }
    public string GetModelId
    {
        get => (GetPropertyValue("id"));
    }
    public string GetAnimations
    {
        get => (GetPropertyValue("animations"));
    }
    public string AppLight
    {
        get => (GetPropertyValue("app_light"));
    }
    public string AppReflaction
    {
        get => (GetPropertyValue("app_reflection"));
    }
    public float ModelLight
    {
        get => float.Parse(GetPropertyValue("model_light"));
    }
    public float ModelReflaction
    {
        get => float.Parse(GetPropertyValue("model_reflection"));
    }

    public string ModelPositionLogo
    {
        get => GetPropertyValue("position_logo");
    }
    public string ModelIsForWall
    {
        get => GetPropertyValue("is_wall", "0");
    }public string ModelSize
    {
        get => GetPropertyValue("size", "1,1,1");
    }
    
    public string ModelPositionPlane
    {
        get => GetPropertyValue("position_plane");
    }
    public string ModelRotateLogo
    {
        get => GetPropertyValue("rotate_logo");
    }
    public string ModelRotatePlane
    {
        get => GetPropertyValue("rotate_plane");
    }
    public string ModelScaleLogo
    {
        get => GetPropertyValue("scale_logo");
    }
    public string ModelScalePlane
    {
        get => GetPropertyValue("scale_plane");
    }
    public string ContactUsUrl
    {
        get => appConfig["contact_us"] as string;
    }

    public string File
    {
        get => GetPropertyValue("file");
    }
    public string GetBundleLink()
    {
    #if PLATFORM_IOS
        return appConfig["ios_path"] as string + File; 
    #elif PLATFORM_ANDROID
        return appConfig["android_path"] as string + File;

    #endif
    }

    private string GetPropertyValue(string property, string defVal = ""){
        print("GetPropertyValue property " +property);
        
        if(!products.ContainsKey(property)) return defVal;
        print("GetPropertyValue Value" + products[property]);
        
        return products[property] as string;
    }
    public float ObjSize
    {
        get {
            return objSize;
        }
    }
    public static Vector3 ConvertStrToVec3(string str)
    {
        string[] strArr = str.Split(',');
        
        return new Vector3(float.Parse(strArr[0]), float.Parse(strArr[1]), float.Parse(strArr[2]));
    }
}
